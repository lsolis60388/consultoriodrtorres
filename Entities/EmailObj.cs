﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class EmailObj
    {

        public string correo { get; set; } = string.Empty;
        public string nombre { get; set; } = string.Empty;

        public string mensaje { get; set; } = string.Empty;

        public string telefono { get; set; } = string.Empty;
    }
}
