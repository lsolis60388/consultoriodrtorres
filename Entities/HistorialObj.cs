﻿using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class HistorialObj
    {
        public int idHistorialClinico { get; set; }
        public string alergiasClinico { get; set; } = string.Empty;

        public string alergiasClinicoDetalle { get; set; } = string.Empty;
        public PacienteObjF pas { get; set; }
        public ProcedemientoClinicoObj pro { get; set; }
        public int idPaciente { get; set; }

        public PacienteObj pasn { get; set; }

        public IEnumerable<ProcedemientoClinicoObj> lista { get; set; }



    }

    public class HistorialObjF
    {
        public int idHistorialClinico { get; set; }
        public string alergiasClinico { get; set; } = string.Empty;
        public int idPaciente { get; set; }
        public string alergiasClinicoDetalle { get; set; } = string.Empty;
        public PacienteObjF pas { get; set; }
        public ProcedemientoClinicoObj pro { get; set; }
        public PacienteObj pasn { get; set; }
    }

    public class ProcedemientoClinicoObj {

        public int idProcedimiento { get; set; }
        public string datalleProcedimientoClinico { get; set; } = string.Empty;
        public string detalleProcedimientoClinicoExtenso { get; set; } = string.Empty;
        public string fechaProcedimientoClinico { get; set; } 

        public int idHistorialClinico { get; set; }
        public IEnumerable<ProcedemientoClinicoObj> lista { get; set; }
    }



    public class ListaProcedimientos
    {
        public string value { get; set; } = string.Empty;

        public Boolean selected { get; set; }

        public int opcion { get; set; } = 0;

        public string procedimiento { get; set; } = string.Empty;
    }


    public class ProcedemientoDienteID
    {

        public string jsonFromDiente { get; set; } = string.Empty;
    }




}
