﻿namespace Entities
{
    public class InventarioObj
    {
        public int idInventario { get; set; }
        public string nombreProductoInventario { get; set; } = string.Empty;
        public int stockSeguridadInventario { get; set; } = int.MinValue;
        public int cantidadActualInventario { get; set; } = int.MinValue;
        public string fechaActualizacion { get; set; } = string.Empty;
        public int idUsuario { get; set; }
    }

    public class InventarioObjF
    {
        public string nombreProductoInventario { get; set; } = string.Empty;
        public int cantidadActualInventario { get; set; } = int.MinValue;
        public string fechaActualizacion { get; set; } = string.Empty;
        public int idUsuario { get; set; }
    }
}
