﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities
{
	public class LoginObj
	{
        public int idUsuario { get; set; }
        [Display(Name = "Nombre")]
        public string nombreUsuario { get; set; } = string.Empty;
        [Required(ErrorMessage = "Digite su correo")]
        [Display(Name = "Correo")]
        [DataType(DataType.EmailAddress)]
        public string correoUsuario { get; set; } = string.Empty;
        [Required(ErrorMessage = "Digite su contraseña")]
        [StringLength(255, ErrorMessage = "Contraseña debe tener mínimo 7 caracteres", MinimumLength = 7)]
        [DataType(DataType.Password)]
        public string passUsuario { get; set; } = string.Empty;
        public int idRolUsuario { get; set; }
        public string olvidarUsuario { get; set; }
        public int intentosUsuario { get; set; }
    }
}

