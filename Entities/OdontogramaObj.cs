﻿using System;
namespace Entities
{
    public class OdontogramaObj
    {
        public int idDiente { get; set; }
        public string numeroDiente { get; set; } = string.Empty;
        public string caraDiente { get; set; } = string.Empty;
        public string procedimientoDiente { get; set; } = string.Empty;
        public string diagnosticoDiente { get; set; } = string.Empty;
        public string fechaCreacion { get; set; } = string.Empty;

    }
}

