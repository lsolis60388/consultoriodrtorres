﻿using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class PacienteObj
    {
        public int idPaciente { get; set; }
        public string nombrePaciente { get; set; }
        public string apellido1Paciente { get; set; }
        public string apellido2Paciente { get; set; }
        public int idHistorialClinicoPaciente { get; set; }
        public string fechaNacimientoPaciente { get; set; }
        public string telefonoPaciente { get; set; }
        public string direccionPaciente { get; set; }
        public string generoPaciente { get; set; }
        public string correoPaciente { get; set; }
        public int idUsuario { get; set; }
        public string nombreCompleto { get; set; }

    }

    public class PacienteObjF
    {
        [Required(ErrorMessage = "Digite su cédula")]
        public int idPaciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string nombrePaciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string apellido1Paciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string apellido2Paciente { get; set; }
        public int idHistorialClinicoPaciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string fechaNacimientoPaciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string telefonoPaciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string direccionPaciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string generoPaciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string correoPaciente { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public int idUsuario { get; set; }
        public string nombreCompleto { get; set; }

        public static implicit operator PacienteObjF(PacienteObj v)
        {
            throw new NotImplementedException();
        }
    }

}
