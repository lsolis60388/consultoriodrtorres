﻿using System.ComponentModel.DataAnnotations;


namespace Entities




{
    public class RecetaObj
    {
        public int idRecetas { get; set; }
        public string detalleReceta { get; set; } = string.Empty;
        public int idHistorialClinico { get; set; }
        public string nombreDoctor { get; set; } = string.Empty;
        public int codigoDoctor { get; set; }

        public string fechaCreacion { get; set; } = string.Empty;

    }

    public class RecetaObjF
    {
        public string detalleReceta { get; set; } = string.Empty;
        public int idHistorialClinico { get; set; }
        public string nombreDoctor { get; set; } = string.Empty;
        public int codigoDoctor { get; set; }
        public string fechaCreacion { get; set; } = string.Empty;
    }



    public class RecetaObjV
    {
        public int idRecetas { get; set; }
        [Display(Name = "ID PACIENTE")]
        public int idPaciente { get; set; }
        public string nombrePaciente { get; set; } = string.Empty;
        [MaxLength(256,ErrorMessage ="Exedio el Limete de Caracteres")]
        [Display(Name = "Detalle Receta")]
        public string detalleReceta { get; set; } = string.Empty;
        public int codigoDoctor { get; set; }
        public string fechaCreacion { get; set; } = string.Empty;
        public string nombreDoctor { get; set; } = string.Empty;
        public int idHistorialClinico { get; set; }
    }


}
