﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities
{
	public class UsuarioObj
	{

        [Required(ErrorMessage = "Cédula es requerida")]
        [RegularExpression(@"^.{9,}$", ErrorMessage = "Cédula no debe contener guiones, mínimo 9 caracteres incluyendo los 0")]
        public int idUsuario { get; set; }
        [Required(ErrorMessage = "Nombre es requerido")]
        [Display(Name = "Nombre")]
        public string nombreUsuario { get; set; } = string.Empty;
        [Required(ErrorMessage = "Correo es requerido")]
        [Display(Name = "Correo")]
        public string correoUsuario { get; set; } = string.Empty;
        [Required(ErrorMessage = "Contraseña es requerida")]
        [StringLength(255, ErrorMessage = "Contraseña debe tener mínimo 7 caracteres", MinimumLength = 7)]
        [DataType(DataType.Password)]
        public string passUsuario { get; set; } = string.Empty;
        [Required(ErrorMessage = "Confirmar contraseña es requerido")]
        [StringLength(255, ErrorMessage = "Contraseña debe tener mínimo 7 caracteres", MinimumLength = 7)]
        [DataType(DataType.Password)]
       [Compare("passUsuario")]
        public string confirmPassword { get; set; } = string.Empty;
        public int idRolUsuario { get; set; }
        public string olvidarUsuario { get; set; } 
        public string TempPassUsuario { get; set; } 
        public string intentosUsuario { get; set; } 
    }
}

