
1.	Como primer paso debemos descargar el SDK de la página de microsoft :Download .NET 5.0 (Linux, macOS, and Windows) , esto nos dará las herramientas necesarias para ejecutar y publicar aplicaciones de Net Core, cuando termine la descarga abrimos el archivo y seguimos la guía de instalación.
  
 

2.	Segundo paso Descargar el Entorno de Desarrollo integrado, en este caso vamos a utilizar Visual Studio 2022 el cual lo descargamos de la url: Visual Studio 2022 Community Edition, lo abrimos y seguimos la guía de instalación.



3.	Antes de iniciar con la parte de subida de nuestros archivos de proyecto a nuestro repositorio es importante tener descargado el software de “Git SCM ( Git - Downloading Package (git-scm.com) ) donde elegimos la plataforma de windows y lo abrimos para instalarlo.




3.1.	Una vez descargado lo ejecutaremos y únicamente le daremos “Next” entre pantallas hasta finalizar su instalación.
 


3.2.	 Lo siguiente a realizar es clonar nuestro proyecto de la nube de Bitbucket para tenerlo de manera local para esto debemos de ir a la opción señalada que dice “Clone” la cual nos desplegará una ventana con un comando el cual debemos de copiarlo al portapapeles.
 

3.3.	Después nos abrirá una consola como el de la imagen y pegaremos el comando que teníamos en el portapapeles  (git clone https://NATHY-C@bitbucket.org/lsolis60388/consultoriodrtorres.git ) y daremos Enter. Se observará el proyecto creado en el Bitbucket se clona en nuestro explorador de Windows.


3.4.	 Se nos desplegará una pestaña donde se debe de ingresar el usuario y la contraseña del bitbuck.
 
3.5.	 En la consola aparecerá un mensaje indicando que ya está correctamente clonado. 

 

3.6.	 Se abre la aplicación de Visual Studio y se seleccionará la opción de “Open a project or solution”. 

3.7.	 Se procede a buscar la carpeta donde se guardó el repositorio y le damos click en abrir. 
 

3.8.	 Por último, se abre el proyecto desarrollado. 
 

Capa de Datos
Entorno de Desarrollo 	MySQL versión 8.0
Lenguaje	SQL 
Sistema Gestor de Base de Datos	MySQL Workbench


Lenguaje	.Net Core 6.0
C# 8.0


