﻿using Dapper;
using System.Data;
using MySql.Data.MySqlClient;
using Entities;
namespace Repository
{
	public class BitacoraTotal : IBitacora
	{


        public void RegistroIngresoSistema(LoginObj usuario)
        {
            try
            {
                using var con = new MySqlConnection(MySQLString.Valor);
                con.Open();
                var transaction = con.BeginTransaction();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@IdUsuariob", usuario.idUsuario);
                    param.Add("@UsuarioErrorb", usuario.nombreUsuario);
                    param.Add("@DetalleErrorb", "INGRESO AL SISTEMA");
                    param.Add("@FechaErrorb", DateTime.Now);
                    var result = con.Execute("SP_Crear_Registro_Usuario", param, transaction, 0, CommandType.StoredProcedure);
                    if (result > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

