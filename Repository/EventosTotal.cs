﻿using System;
using System.Data;
using Dapper;
using Entities;
using MySql.Data.MySqlClient;

namespace Repository
{
	public class EventosTotal : IEventos
	{
        public string listaEventos()
        {
            return null;
        }

        public IEnumerable<EventosObj> ObtenerListaEventos()
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var respuesta = con.Query<EventosObj>("SP_Consultar_Eventos", null, null, true, 0, CommandType.StoredProcedure).ToList();

            return respuesta;
        }


        public void InsertarEvento(EventosObj evento)
        {
            try
            {
                using var con = new MySqlConnection(MySQLString.Valor);
                con.Open();
                var transaction = con.BeginTransaction();
                try
                {
                    var param = new DynamicParameters();

                    param.Add("@IdHistorialClinicoa", evento.IdHistorialClinico);
                    param.Add("@IdUsuarioa", evento.IdUsuario);
                    param.Add("@titlea", evento.title);
                    param.Add("@allDaya", evento.allDay);
                    param.Add("@groupIda", evento.groupId);
                    param.Add("@colora", evento.color);
                    param.Add("@starta", evento.start);
                    param.Add("@enda", evento.end);
                    param.Add("@descrip", evento.description);

                    var result = con.Execute(" SP_Crear_Eventos", param, transaction, 0, CommandType.StoredProcedure);
                    if (result > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception faru)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ActualizarEvento(EventosObj evento)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@IdHistorialClinico", evento.IdHistorialClinico);
                param.Add("@IdUsuario", evento.IdUsuario);
                param.Add("@TitleEventos", evento.title);
                param.Add("@StartEventos", evento.start);
                param.Add("@EndEventos", evento.end);
              
                param.Add("@IdEventosT", evento.id);
                var result = con.Execute("SP_Actualizar_Evento", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception faru)
            {
                transaction.Rollback();
            }
        }

        // Valida si existe algo de insumo con ese ID
        public bool VerificarEventoExiste(int idEvento)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@eventoId", idEvento);
            var result = con.Query<bool>("SP_Consultar_Eventos", param, null, false, 0, CommandType.StoredProcedure).FirstOrDefault();
            return result;
        }

        // Eliminar
        public void EliminarEvento(int idEvento)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@idEvento", idEvento);
            var result = con.Execute("SP_Eliminar_Evento", param, null, 0, CommandType.StoredProcedure);
        }

        public RecetaObj ObtenerEventoId(int eventoId)
        {



            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@idEvento", eventoId);
            return con.Query<RecetaObj>("SP_Obtener_Evento_ID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }

        EventosObj IEventos.ObtenerEventoId(int eventoId)
        {



            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@idEvento", eventoId);
            return con.Query<EventosObj>("SP_Obtener_Evento_ID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();


            throw new NotImplementedException();

        }

      

        IEnumerable<EventosObj> IEventos.ObtenerEventoIdPaciente(string eventoId)
        {

            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@pacienteId", eventoId);
            return con.Query <EventosObj>("SP_Obtener_Eventos_IdPaciente", param, null, true, 0, CommandType.StoredProcedure).ToList();




        }
    }
}

