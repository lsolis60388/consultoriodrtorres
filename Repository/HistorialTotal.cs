﻿using Entities;
using Dapper;
using MySql.Data.MySqlClient;

using System.Data;


namespace Repository
{
    public class HistorialTotal: Ihistorialclinico
    {
        public void ActualizarHistorial(HistorialObj historial)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@alergiasClinico", historial.alergiasClinico);
                param.Add("@idhistorial", historial.idHistorialClinico);
                param.Add("@idPaciente", historial.idPaciente);

                var result = con.Execute("SP_Actualizar_Historial", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
            }
        }

        // Valida si existe algo de insumo con ese ID
        public bool VerificarHistorialExiste(int idHistorialClinico)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@historialId", idHistorialClinico);
            var result = con.Query<bool>("SP_Consultar_Historial", param, null, false, 0, CommandType.StoredProcedure).FirstOrDefault();
            return result;
        }

        // Eliminar
        public void EliminarHistorial(int historialId)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@historialId", historialId);
            var result = con.Execute("SP_Eliminar_Historial", param, null, 0, CommandType.StoredProcedure);
        }

        //  Insertar
        public void InsertarHistorial(HistorialObjF historial)
        {
            try
            {
                using var con = new MySqlConnection(MySQLString.Valor);
                con.Open();
                var transaction = con.BeginTransaction();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@alergiasClinicoc", historial.alergiasClinico);
                    param.Add("@idPacientec", historial.idPaciente);
                    
                  

                    var result =  con.Execute("SP_Insertar_Historial_Clinico", param, transaction, 0, CommandType.StoredProcedure);
                    if (result > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //  Obtener lista de productos
        public IEnumerable<HistorialObj> ObtenerHistorial()
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var respuesta = con.Query<HistorialObj>("SP_Consultar_Historial", null, null, true, 0, CommandType.StoredProcedure).ToList();



            return respuesta;
        }


        //  Obtener un insumo por ID
        public HistorialObj ObtenerHistorialId(int historialId)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@historialId", historialId);
            return con.Query<HistorialObj>("SP_Obtener_Historial_ID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }

        public IEnumerable<HistorialObj> ObtenerVistaHistorial()
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var respuesta = con.Query<HistorialObj>("SP_Historial", null, null, true, 0, CommandType.StoredProcedure).ToList();



            return respuesta;
        }
        //Procedimiento Clinico
        public ProcedemientoClinicoObj ObtenerProcedimientoId(int procedimeintoId)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@procedimientoId", procedimeintoId);
            return con.Query<ProcedemientoClinicoObj>("SP_Obtener_Historial_ID_PACIENTE", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }

        public void InsertarProcedimiento(ProcedemientoClinicoObj procedimiento)
        {
            try
            {
                using var con = new MySqlConnection(MySQLString.Valor);
                con.Open();
                var transaction = con.BeginTransaction();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@detalleProcedimientoClinico", procedimiento.datalleProcedimientoClinico);
                    param.Add("@idHistorialClinico", procedimiento.idHistorialClinico);


                    var result = con.Execute("SP_Crear_Procedimiento_Faru_dos", param, transaction, 0, CommandType.StoredProcedure);
                    if (result > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ActualizarProcedimiento(ProcedemientoClinicoObj procedimiento)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@detalleProcedimientoClinico", procedimiento.datalleProcedimientoClinico);
                param.Add("@idHistorialClinico", procedimiento.idHistorialClinico);

                var result = con.Execute("SP_Actualizar_Procedimiento", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                transaction.Rollback();
            }
        }
      

      
        public IEnumerable<ListaProcedimientos> listaProcedimientos()
        {

            List<ListaProcedimientos> lista = new List<ListaProcedimientos>();
            lista.Add(new ListaProcedimientos() { value = "option1", procedimiento = "Limpieza Dental",selected=false });
            lista.Add(new ListaProcedimientos() { value = "option1", procedimiento = "Valoracion", selected = false });
            lista.Add(new ListaProcedimientos() { value = "option2", procedimiento = "Blanqueamiento Dental", selected = false });
            lista.Add(new ListaProcedimientos() { value = "option3", procedimiento = "Tratamiento Dental", selected = false });
            lista.Add(new ListaProcedimientos() { value = "option4", procedimiento = "Cirugía", selected = false });

            return lista;
        }

        public IEnumerable<ProcedemientoClinicoObj> ObtenerProcedimientoIdPaciente(PacienteObjF proc)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@pacienteId", proc.idPaciente);
            return con.Query<ProcedemientoClinicoObj>("SP_Obtener_Procedimientos_IdPaciente", param, null, true, 0, CommandType.StoredProcedure);
        }

        //  Obtener un insumo por ID para el procedimiento
        public ProcedemientoClinicoObj ObtenerProcedimientoDienteID(int procedimientoId)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@procedimientoIdV", procedimientoId);
            return con.Query<ProcedemientoClinicoObj>("SP_Obtener_Procedimientos_DientesID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }

        //  Obtener un insumo por ID para el procedimiento
        public ProcedemientoDienteID ObtenerDetalleProcedimientoDientesID(int idHistorialClinico)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
               var param = new DynamicParameters();
               param.Add("@idHistorialU", idHistorialClinico);
               return con.Query<ProcedemientoDienteID>("SP_Dientes_View_Odontograma", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }

    }
}
