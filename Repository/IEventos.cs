﻿using System;
using Entities;

namespace Repository
{
	public interface IEventos
	{
		IEnumerable<EventosObj> ObtenerListaEventos(); // R
		void InsertarEvento(EventosObj evento); // C
		EventosObj ObtenerEventoId(int eventoId); // R
		IEnumerable<EventosObj> ObtenerEventoIdPaciente(string eventoId); // R

		void ActualizarEvento(EventosObj evento); //U

		void EliminarEvento(int eventoId); //D
		bool VerificarEventoExiste(int id);
		string listaEventos();

	}
}

