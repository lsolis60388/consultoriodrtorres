﻿using Entities;
namespace Repository
{
	public interface ILogin
	{
        LoginObj ObtenerUsuarioByID(LoginObj loginObj); // R

        LoginObj ObtenerCorreoUsuarioId(LoginObj loginObj); // R

        void ActualizarIntento(string email, int intentos); //U

        void ActualizarCambiarPassBloqueado(string email); //U

    }
}

