﻿using System.Collections.Generic;
using Entities;


namespace Repository
{
    public interface IPaciente
	{
        void InsertarPaciente(PacienteObjF paciente); // C

        IEnumerable<PacienteObj> ObtenerPaciente();

        //faru
        IEnumerable<PacienteObj> ObtenerPacienteCalendario();//paciente de calendario
        //
        PacienteObj ObtenerPacienteId(int pacienteId); // R

        void ActualizarPaciente(PacienteObj paciente); //U

        void EliminarPaciente(int pacienteId); //D
                                               // bool VerificarInsumoExiste(int id);
    }
}

