﻿using Entities;
namespace Repository
{
	public interface IUsuario
	{
        void CrearUsuario(UsuarioObj usuario); // C
        UsuarioObj OlvidoContrasenna(UsuarioObj usuarioObj); // R
        UsuarioObj ObtenerCorreoID(UsuarioObj usuarioObj); // R
        void CambiarContrasenna(UsuarioObj usuarioObj, string mail);
    }
}

