﻿using Entities;

namespace Repository
{
    public interface Ihistorialclinico 
    {
        void InsertarHistorial(HistorialObjF historial); // C

        IEnumerable<HistorialObj> ObtenerHistorial(); // R
        IEnumerable<HistorialObj> ObtenerVistaHistorial(); // R
        IEnumerable<ListaProcedimientos> listaProcedimientos(); // R IEnumerable<HistorialObj> ObtenerHistorial(); // R




        HistorialObj ObtenerHistorialId(int historialId); // R


        void ActualizarHistorial(HistorialObj historial); //U

        void EliminarHistorial(int historialId); //D
        bool VerificarHistorialExiste(int id);

        void InsertarProcedimiento(ProcedemientoClinicoObj procedemiento); // C

        void ActualizarProcedimiento(ProcedemientoClinicoObj procedemiento); //U
        ProcedemientoClinicoObj ObtenerProcedimientoId(int procedimientoId);
        public IEnumerable<ProcedemientoClinicoObj> ObtenerProcedimientoIdPaciente(PacienteObjF proc);// R

        ProcedemientoClinicoObj ObtenerProcedimientoDienteID(int procedimientoId);

        ProcedemientoDienteID ObtenerDetalleProcedimientoDientesID(int idHistorialClinico);

    }
}
