﻿using System.Collections.Generic;
using Entities;

namespace Repository
{
    public interface Iinventario
    {

        void InsertarInsumo(InventarioObjF inventario, int idUserI); // C

        IEnumerable<InventarioObj> ObtenerInsumos(); // R

        InventarioObj ObtenerInventarioInsumoId(int inventarioId); // R

        void ActualizarInsumo(InventarioObj inventario); //U

        void EliminarInsumo(int inventarioId); //D
       // bool VerificarInsumoExiste(int id);
    }
}
