﻿using Dapper;
using System.Data;
using MySql.Data.MySqlClient;
using Entities;

namespace Repository
{
    public class InventarioTotal : Iinventario
    { 
         //  Insertar
        public void InsertarInsumo(InventarioObjF inventario, int idUserI)
    {
        try
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@nombreProductoInventarioV", inventario.nombreProductoInventario);
                param.Add("@cantidadActualInventarioV", inventario.cantidadActualInventario);
                param.Add("@idUsuarioV", idUserI);
                var result = con.Execute("SP_Crear_Insumo", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                transaction.Rollback();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }


    // Actualizar
    public void ActualizarInsumo(InventarioObj inventario)
    {
        using var con = new MySqlConnection(MySQLString.Valor);
        con.Open();
        var transaction = con.BeginTransaction();
        try
        {
            var param = new DynamicParameters();
            param.Add("@nombreProductoInventarioV", inventario.nombreProductoInventario);
            param.Add("@cantidadActualInventarioV", inventario.cantidadActualInventario);
            param.Add("@inventarioId", inventario.idInventario);
            var result = con.Execute("SP_Actualizar_Insumo", param, transaction, 0, CommandType.StoredProcedure);
            if (result > 0)
            {
                transaction.Commit();
            }
        }
        catch (Exception)
        {
            transaction.Rollback();
        }
    }

    //  Obtener lista de productos
    public IEnumerable<InventarioObj> ObtenerInsumos()
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            return con.Query<InventarioObj>("SP_Consultar_Inventario", null, null, true, 0, CommandType.StoredProcedure).ToList();
        }

        // Eliminar
        public void EliminarInsumo(int inventarioId)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@inventarioId", inventarioId);
            var result = con.Execute("SP_Eliminar_Insumo", param, null, 0, CommandType.StoredProcedure);
        }

        //  Obtener un insumo por ID
        public InventarioObj ObtenerInventarioInsumoId(int inventarioId)
        {

           
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@inventarioId", inventarioId);
            return con.Query<InventarioObj>("SP_Obtener_Usuario_ByID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}

