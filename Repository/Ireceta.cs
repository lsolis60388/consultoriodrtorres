﻿using Entities;
using System.Collections.Generic;
namespace Repository
{
    public interface Ireceta
    {
        void InsertarReceta(RecetaObjF receta, int idUserI); // C
        IEnumerable<RecetaObj> ObtenerReceta(); // R
        IEnumerable<RecetaObjV> ObtenerVistaReceta(int userIDF); // R
        RecetaObj ObtenerRecetaId(int recetasId); // R
        void ActualizarReceta(RecetaObj receta); //U
        void EliminarReceta(int recetasId); //D
        bool VerificarRecetaExiste(int id);
    }
}
