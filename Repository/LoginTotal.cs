﻿using Dapper;
using System.Data;
using MySql.Data.MySqlClient;
using Entities;
using DocumentFormat.OpenXml.Bibliography;

namespace Repository
{
    public class LoginTotal : ILogin
    {

        public void ActualizarCambiarPassBloqueado(string email)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@loginCorreoUsuarioV", email);
                var result = con.Execute("SP_Actualizar_CambiarPassUsuario_ByID", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
            }
        }

        public void ActualizarIntento(string email, int intentos)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@loginCorreoUsuarioV", email);
                param.Add("@loginIntentosUsuarioV", intentos);
                var result = con.Execute("SP_Actualizar_IntentosUsuario_ByID", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
            }
        }

        public LoginObj ObtenerCorreoUsuarioId(LoginObj loginObj)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@loginCorreoUsuarioV", loginObj.correoUsuario);
            return con.Query<LoginObj>("SP_Obtener_correoUsuario_ByID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }

        public LoginObj ObtenerUsuarioByID(LoginObj loginObj)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@correoUsuarioV", loginObj.correoUsuario);
            param.Add("@passUsuarioV", loginObj.passUsuario);
            return con.Query<LoginObj>("SP_Obtener_LoginUsuario_ByCorreo", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}

