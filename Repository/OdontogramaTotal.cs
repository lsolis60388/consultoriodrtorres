﻿using System;
using Dapper;
using Entities;
using MySql.Data.MySqlClient;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Repository
{
    public class OdontogramaTotal : IOdontograma
    {
        public void InsertarOdontograma(string jsonData, string idUsaurio)
        {
            try
            {
                using var con = new MySqlConnection(MySQLString.Valor);
                con.Open();
                var transaction = con.BeginTransaction();
                try
                {
                    var param = new DynamicParameters();
                    jsonData = jsonData.Replace("[", string.Empty).Replace("]", string.Empty);
                    param.Add("@idUsaurioV", idUsaurio);
                    param.Add("@DATOS", jsonData);
                    var result = con.Execute("SP_Crear_Odontograma_Paciente", param, transaction, 0, CommandType.StoredProcedure);
                    if (result > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

