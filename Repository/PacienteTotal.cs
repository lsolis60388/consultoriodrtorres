﻿using Dapper;
using System.Data;
using MySql.Data.MySqlClient;
using Entities;

namespace Repository
{
    public class PacienteTotal : IPaciente
    {
        //  Insertar
        public void InsertarPaciente(PacienteObjF paciente)
        {
            try
            {
                using var con = new MySqlConnection(MySQLString.Valor);
                con.Open();
                var transaction = con.BeginTransaction();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@idPacienteV", paciente.idPaciente);
                    param.Add("@nombrePacienteV", paciente.nombrePaciente);
                    param.Add("@apellido1PacienteV", paciente.apellido1Paciente);
                    param.Add("@apellido2PacienteV", paciente.apellido2Paciente);
                    param.Add("@idHistorialClinicoPacienteV", paciente.idHistorialClinicoPaciente);
                    param.Add("@fechaNacimientoPacienteV", paciente.fechaNacimientoPaciente);
                    param.Add("@telefonoPacienteV", paciente.telefonoPaciente);
                    param.Add("@direccionPacienteV", paciente.direccionPaciente);
                    param.Add("@generoPacienteV", paciente.generoPaciente);
                    param.Add("@correoPacienteV", paciente.correoPaciente);
                    param.Add("@idUsuarioV", null);
                    var result = con.Execute("SP_Crear_Paciented", param, transaction, 0, CommandType.StoredProcedure);
                    if (result > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        // Actualizar
        public void ActualizarPaciente(PacienteObj paciente)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@nombrepacienteV", paciente.nombrePaciente);
                param.Add("@apellido1PacienteV", paciente.apellido1Paciente);
                param.Add("@apellido2PacienteV", paciente.apellido2Paciente);
                param.Add("@idHistorialClinicoPacienteV", paciente.idHistorialClinicoPaciente);
                param.Add("@fechaNacimientoPacienteV", paciente.fechaNacimientoPaciente);
                param.Add("@telefonoPacienteV", paciente.telefonoPaciente);
                param.Add("@direccionPacienteV", paciente.direccionPaciente);
                param.Add("@generoPacienteV", paciente.generoPaciente);
                param.Add("@correoPacienteV", paciente.correoPaciente);
                param.Add("@idPacienteV", paciente.idPaciente);
                var result = con.Execute("SP_Actualizar_Paciente", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
            }
        }

        //  Obtener lista de pacientes
        public IEnumerable<PacienteObj> ObtenerPaciente()
        {
          using var con = new MySqlConnection(MySQLString.Valor);
            return con.Query<PacienteObj>("SP_Consultar_Paciente", null, null, true, 0, CommandType.StoredProcedure).ToList();
        }

        // Eliminar
        public void EliminarPaciente(int pacienteId)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@pacienteIdV", pacienteId);
            var result = con.Execute("SP_Eliminar_Paciente", param, null, 0, CommandType.StoredProcedure);
        }

        //  Obtener un paciente por ID
        public PacienteObj ObtenerPacienteId(int pacienteId)
        {


            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@pacienteIdV", pacienteId);
            var result = con.Query<PacienteObj>("SP_Obtener_Paciente_ByID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
            return result;



        }

        public IEnumerable<PacienteObj> ObtenerPacienteCalendario()
        {



            using var con = new MySqlConnection(MySQLString.Valor);
            return con.Query<PacienteObj>("SP_Consulta_Paciente_Calendario", null, null, true, 0, CommandType.StoredProcedure).ToList();





        }
    }
    }

