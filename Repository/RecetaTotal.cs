﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using Entities;
namespace Repository
{
    public class RecetaTotal : Ireceta
    {
        public void ActualizarReceta(RecetaObj receta)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@nombreDoctor", receta.nombreDoctor);
                param.Add("@codigoDoctor", receta.codigoDoctor);
                param.Add("@detalleReceta", receta.detalleReceta);
                param.Add("@idHistorialClinico", receta.idHistorialClinico);
                param.Add("@idRecetasT", receta.idRecetas);
                var result = con.Execute("SP_Actualizar_Receta", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                transaction.Rollback();
            }
        }
        // Valida si existe algo de insumo con ese ID
        public bool VerificarRecetaExiste(int idRecetas)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@recetasId", idRecetas);
            var result = con.Query<bool>("SP_Consultar_Recetas", param, null, false, 0, CommandType.StoredProcedure).FirstOrDefault();
            return result;
        }
        // Eliminar
        public void EliminarReceta(int recetasId)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@recetasId", recetasId);
            var result = con.Execute("SP_Eliminar_Receta", param, null, 0, CommandType.StoredProcedure);
        }
        //  Insertar
        public void InsertarReceta(RecetaObjF receta, int idUserI)
        {
            try
            {
                using var con = new MySqlConnection(MySQLString.Valor);
                con.Open();
                var transaction = con.BeginTransaction();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@nombreDoctor", receta.nombreDoctor);
                    param.Add("@codigoDoctor", receta.codigoDoctor);
                    param.Add("@detalleReceta", receta.detalleReceta);
                    param.Add("@idHistorialClinico", idUserI);
                    var result = con.Execute("SP_Crear_Receta", param, transaction, 0, CommandType.StoredProcedure);
                    if (result > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //  Obtener lista de productos
        public IEnumerable<RecetaObj> ObtenerReceta()
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var respueta = con.Query<RecetaObj>("SP_Consultar_Recetas", null, null, true, 0, CommandType.StoredProcedure).ToList();
            return respueta;
        }
        //  Obtener un insumo por ID
        public RecetaObj ObtenerRecetaId(int recetasId)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@recetasId", recetasId);
#pragma warning disable CS8603 // Posible tipo de valor devuelto de referencia nulo
            return con.Query<RecetaObj>("SP_Obtener_Recetas_ID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
#pragma warning restore CS8603 // Posible tipo de valor devuelto de referencia nulo
        }
        public IEnumerable<RecetaObjV> ObtenerVistaReceta(int userIDF)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@userF", userIDF);
            var respueta = con.Query<RecetaObjV>("SP_RECETA", param, null, true, 0, CommandType.StoredProcedure).ToList();
            return respueta;
        }
    }
}

