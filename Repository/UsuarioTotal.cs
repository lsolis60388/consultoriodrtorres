﻿using Dapper;
using System.Data;
using MySql.Data.MySqlClient;
using Entities;
namespace Repository
{
	public class UsuarioTotal : IUsuario
	{

        public void CrearUsuario(UsuarioObj usuario)
        {
            try
            {
                using var con = new MySqlConnection(MySQLString.Valor);
                con.Open();
                var transaction = con.BeginTransaction();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@cedulaV", usuario.idUsuario);
                    param.Add("@nombreUsuarioV", usuario.nombreUsuario);
                    param.Add("@correoUsuarioV", usuario.correoUsuario);
                    param.Add("@passUsuarioV", usuario.passUsuario);
                    var result = con.Execute("SP_Crear_UsuarioLogin", param, transaction, 0, CommandType.StoredProcedure);
                    if (result > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        // Obtener correo o Cédula
        public UsuarioObj ObtenerCorreoID(UsuarioObj usuarioObj)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@idUsuarioV", usuarioObj.idUsuario);
            param.Add("@correoUsuarioV", usuarioObj.correoUsuario);
            return con.Query<UsuarioObj>("SP_Obtener_CrearUsuario_ByCorreoID", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }

        public UsuarioObj OlvidoContrasenna(UsuarioObj usuarioObj)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            var param = new DynamicParameters();
            param.Add("@correoUsuarioV", usuarioObj.correoUsuario);
            return con.Query<UsuarioObj>("SP_Obtener_OlvidarContrasenna_ByCorreo", param, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
        }

        public void CambiarContrasenna(UsuarioObj usuarioObj, string mail)
        {
            using var con = new MySqlConnection(MySQLString.Valor);
            con.Open();
            var transaction = con.BeginTransaction();
            try
            {
                var param = new DynamicParameters();
                param.Add("@passUsuarioV", usuarioObj.passUsuario);
                param.Add("@correoUsuarioV", mail);
                var result = con.Execute("SP_Cambiar_Contrasenna_Usuario", param, transaction, 0, CommandType.StoredProcedure);
                if (result > 0)
                {
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                transaction.Rollback();
            }
        }
    }
}

