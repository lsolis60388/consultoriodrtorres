﻿using System.Net.Mail;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository;
using System.Data;
using System.Linq;
using System.Security.Claims;
using DocumentFormat.OpenXml.InkML;
using Newtonsoft.Json;

namespace Ui.Controllers
{
   [Authorize]
    public class CalendarioController : Controller
    {

        private IEnumerable<PacienteObj> opc;


        private readonly IEventos  _IEventos;
       
        private readonly IPaciente _Paciente;
        public CalendarioController(IEventos eventos, IPaciente paciente)
        {
            _IEventos = eventos; //Inyeccion de dependencias
            _Paciente = paciente; //Inyeccion de dependencias

        }


        [Produces("application/json")]
        public IActionResult SearchUser(string term = null)
        {
           var Data = _Paciente.ObtenerPacienteCalendario();


           var result = Data
                .Where(u => u.nombreCompleto.ToLower().Contains(term.ToLower()))
                .ToList();
            return Json(result);
        }

        public IActionResult IndexPrueba()
        {

            return View();
        }


        [HttpGet]
        public IActionResult Index1()
        {
            ClaimsPrincipal currentUser = User;
            var identityl = (System.Security.Claims.ClaimsIdentity)User.Identity;
            string idf = identityl.Claims.FirstOrDefault(c => c.Type == "id").Value;


            var events = _IEventos.ObtenerEventoIdPaciente(idf);


            return View(events);

        }

            [HttpGet]
            public IActionResult GetCalendarEvents(string start, string end)
            {
                var events = _IEventos.ObtenerListaEventos();

            return Json(events);
            }

        //Agregar Eventos

            [HttpPost]
            public IActionResult AddEvent([FromBody] EventosObj data)


            {
                string message = String.Empty;
            EmailObj em = new EmailObj();
            if(em.correo == null | em.correo =="") {

                string mail = HttpContext.Session.GetString("Correo");
                em.correo = mail;
            }
            else { em.correo = data.correo; }
               

          

            _IEventos.InsertarEvento(data);
            sendEmail(data.start, data.end,em);


            return Json(new { message });
            }

        
        //Elimina Eventos
        [HttpPost]
            public IActionResult DeleteEvent([FromBody] Event evt)
            {
            string message = String.Empty;

            var events = _IEventos.ObtenerEventoId(evt.EventId);
            if (events != null)
            {
                
                _IEventos.EliminarEvento(evt.EventId);

            }

            return Json(new { message });
            }



        //Obtiene los Eventos
        public JsonResult GetEvents()
        {

            var events = _IEventos.ObtenerListaEventos();


            
            if (User.IsInRole("2"))
            {
                //events.ToList().ForEach(c => c.display = "background");
                events.ToList().ForEach(c => c.title = "");

            }


            return Json(events);
        }


        //Obtienen Paciente 
        public JsonResult GetPatient(string evt)
        {

            var Data = _Paciente.ObtenerPacienteCalendario();

            evt = "Eduardo";



            var re = (from faru in Data where faru.nombreCompleto.Contains(evt) select faru).FirstOrDefault();


            return Json(re);
        }




        [Produces("application/json")]
        public IActionResult SearchPatient(string term)
        {
            var Data = _Paciente.ObtenerPacienteCalendario();


            var result = (from faru in Data where faru.nombreCompleto.Contains(term) select faru).FirstOrDefault();
            return Json(result);
        }




        [HttpGet]
        public JsonResult SearchPatient1(int id)
        {

            var Data = _Paciente.ObtenerPacienteId(id);
            Data.nombreCompleto = Data.nombrePaciente + " " + Data.apellido1Paciente + " " + Data.apellido2Paciente;


            //var result = (from faru in Data where faru.nombreCompleto.Contains(id.ToString()) select faru).FirstOrDefault();
            return Json(Data);
          
        }





        public void sendEmail(DateTime start, DateTime end, EmailObj email)
        {

            SmtpClient smtp = new SmtpClient("smtp.office365.com");

            smtp.Port = 587;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            System.Net.NetworkCredential credential =
                new System.Net.NetworkCredential("notificaciones@torresdent.com", "3teshe-ZY");
            smtp.EnableSsl = true;
            smtp.Credentials = credential;

            MailMessage mensaje = new MailMessage("notificaciones@torresdent.com", email.correo);
            mensaje.Subject = "Nueva cita agendada - Clínica Dr Torres";
            mensaje.Body = String.Format("<p>Estimado paciente,</p>\n<p>Se ha agendado una nueva cita:</p>\n<p>Inicio: {0}</p>\n<p>Finalización: {1}</p>\n<p>En el siguiente enlace podr&aacute; ingresar a la p&aacute;gina web: <a title=\"Cl&iacute;nica Dr Torres\" href=\"https://www.torresdent.com/login/index\" target=\"_blank\">Cl&iacute;nica Dr Torres</a></p>", start, end);
            mensaje.IsBodyHtml = true;
            smtp.Send(mensaje);

        }




    }




}



    
