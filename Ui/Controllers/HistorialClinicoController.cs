﻿using Microsoft.AspNetCore.Mvc;
using Entities;
using Repository;
using X.PagedList;
using ClosedXML.Excel;
using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

using System.Globalization;
using System.Security.Claims;
namespace Ui.Controllers
{

    [Authorize(Roles = "1")]
    public class HistorialClinicoController : Controller
    {
        private readonly Ihistorialclinico _Historial;
        private readonly IPaciente _Paciente;
        public HistorialClinicoController(Ihistorialclinico historial, IPaciente paciente)
        {
            _Historial = historial;
            _Paciente = paciente;
        }
        public IActionResult Index(PacienteObjF id)
        {
            var idp = id.idPaciente;
            var historial = _Historial.ObtenerHistorialId(id.idPaciente);
            if (historial == null)
            {
                CrearHistorialClinico(id);
                historial = _Historial.ObtenerHistorialId(id.idPaciente);
            }
            ProcedemientoClinicoObj proc = new ProcedemientoClinicoObj();
            historial.pro = proc;
            historial.pro.idHistorialClinico = historial.idHistorialClinico;
            historial.pasn = _Paciente.ObtenerPacienteId(idp);
            historial.lista = _Historial.ObtenerProcedimientoIdPaciente(id);
            var list = _Historial.listaProcedimientos().Select(x => new SelectListItem() { Value = x.procedimiento, Text = x.procedimiento }).ToList();
            ViewBag.Region = _Historial.listaProcedimientos().Select(x => new SelectListItem() { Value = x.procedimiento, Text = x.procedimiento }).ToList();
            ViewData["lista"] = list;
            return View(historial);
        }
        private void CrearHistorialClinico(PacienteObjF p)
        {
            var hc = new HistorialObjF();
            hc.idHistorialClinico = p.idPaciente;
            hc.idPaciente = p.idPaciente;
            hc.alergiasClinicoDetalle = " ";
            _Historial.InsertarHistorial(hc);
        }
        public IActionResult Editar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var historial = _Historial.ObtenerHistorialId(Convert.ToInt32(id));
            return View(historial);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Editar(int id, [Bind("alergiasClinico, idHistorial")] HistorialObj historial)
        {
            if (id != historial.idHistorialClinico)
            {
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _Historial.ActualizarHistorial(historial);
                }
                catch (Exception)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(historial);
        }
        public IActionResult Eliminar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var historial = _Historial.ObtenerHistorialId(Convert.ToInt32(id));
            if (historial == null)
            {
                return RedirectToAction("Index");
            }
            return View(historial);
        }
        public IActionResult Crear()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CrearHistorialClinico(HistorialObj historial)
        {
            // if (!ModelState.IsValid) return BadRequest("Enter required fields");
            var x = historial;
            x.alergiasClinico = x.alergiasClinico + " " + x.alergiasClinicoDetalle;
            _Historial.ActualizarHistorial(historial);
            return this.Ok("Form Data received!");
        }
        [HttpPost]
        public IActionResult CrearprocedimientoClinico(ProcedemientoClinicoObj procedimientoClinico)
        {
            // if (ModelState.IsValid)
            //{
            var x = procedimientoClinico;
            x.datalleProcedimientoClinico = x.datalleProcedimientoClinico + " " + x.detalleProcedimientoClinicoExtenso;
            _Historial.InsertarProcedimiento(x);
            // }
            return this.Ok("Form Data received!");
        }
        [HttpPost, ActionName("Eliminar")]
        [ValidateAntiForgeryToken]
        public IActionResult Eliminar(int id)
        {
            _Historial.EliminarHistorial(id);
            return RedirectToAction(nameof(Index));
        }
        public IActionResult Detalles(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var historial = _Historial.ObtenerHistorialId(Convert.ToInt32(id));
            if (historial == null)
            {
                return RedirectToAction("Index");
            }
            return View(historial);
        }
        //ProcedimientoClinico
        public IActionResult CrearProcedimiento()
        {
            return View();
        }
        public IActionResult EditarProcedimiento(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var historial = _Historial.ObtenerProcedimientoId(Convert.ToInt32(id));
            return View(historial);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditarProcedmiento(int id, [Bind("detalleProcedimientoClinico,idHistorialClinico")] ProcedemientoClinicoObj procedemiento)
        {
            if (id != procedemiento.idProcedimiento)
            {
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _Historial.ActualizarProcedimiento(procedemiento);
                }
                catch (Exception)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(procedemiento);
        }
        // Procedimeinto para traer los parametros y los pasa al odontograma
        public IActionResult ProcedimientoID(int id)
        {
            var procedimiento = _Historial.ObtenerProcedimientoDienteID(Convert.ToInt32(id));
            var idUsuarioF = _Paciente.ObtenerPacienteId(procedimiento.idHistorialClinico);
            var Detalleprocedimiento = _Historial.ObtenerDetalleProcedimientoDientesID(procedimiento.idProcedimiento);
            HttpContext.Session.SetString("UserName", string.Concat(idUsuarioF.nombrePaciente + " " + idUsuarioF.apellido1Paciente + " " + idUsuarioF.apellido2Paciente).ToString());

            HttpContext.Session.SetString("idUsuario", procedimiento.idProcedimiento.ToString());
            HttpContext.Session.SetString("stringValues", Detalleprocedimiento.jsonFromDiente.ToString());
            return RedirectToAction("Index", "Odontograma");
        }
        // Procedimeinto para traer los parametros y los pasa al Recetas
        public IActionResult ProcedimientoRecetasID(int id)
        {
            var procedimiento = _Historial.ObtenerProcedimientoDienteID(Convert.ToInt32(id));
            var idUsuarioF = _Paciente.ObtenerPacienteId(procedimiento.idHistorialClinico);
            //var Detalleprocedimiento = _Historial.ObtenerDetalleProcedimientoDientesID(procedimiento.fechaProcedimientoClinico, procedimiento.idHistorialClinico);
            HttpContext.Session.SetString("UserName", string.Concat(idUsuarioF.nombrePaciente + " " + idUsuarioF.apellido1Paciente + " " + idUsuarioF.apellido2Paciente).ToString());
            HttpContext.Session.SetString("idUsuario", procedimiento.idHistorialClinico.ToString());
            //HttpContext.Session.SetString("stringValues", Detalleprocedimiento.jsonFromDiente.ToString());
            return RedirectToAction("Index", "Receta");
        }
    }
}




