﻿using System.Diagnostics;
using System.Net.Mail;
using Microsoft.AspNetCore.Mvc;
using Entities;
using Ui.Models;


namespace Ui.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult EnviarEmail()
        {
            return RedirectToAction("EnviarEmail", "Home");
        }

        [HttpPost]
        public IActionResult EnviarEmail(EmailObj email)
        {

            if (email != null)
            {

                SmtpClient smtp = new SmtpClient("smtp.office365.com");

                smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credential =
                    new System.Net.NetworkCredential("notificaciones@torresdent.com", "3teshe-ZY");
                smtp.EnableSsl = true;
                smtp.Credentials = credential;

                MailMessage mensaje = new MailMessage("notificaciones@torresdent.com", email.correo);
                mensaje.Subject = "Correo de Contacto";
                mensaje.Body = String.Format("<p>Estimado(a),</p><p>{0}</p>\n<p>Teléfono: </p><p>{1}</p>\n<p>Correo: </p><p>{2}</p>\n\n<p>{3}</p>", email.nombre, email.telefono,email.correo,email.mensaje);
                mensaje.IsBodyHtml = true;
                smtp.Send(mensaje);

                return Redirect("~/Home/Index");
            }
            else
            {
                ViewBag.msg = "Error, el correo no existe";
                return View();
            }
        }

        //Test

    }
}
