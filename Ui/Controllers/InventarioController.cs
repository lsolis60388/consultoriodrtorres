﻿using Microsoft.AspNetCore.Mvc;

using Entities;
using Repository;
using X.PagedList;
using ClosedXML.Excel;
using System.Data;

using Microsoft.AspNetCore.Authorization;

namespace Ui.Controllers
{
    [Authorize(Roles ="1")]
    public class InventarioController : Controller
    {
        private readonly Iinventario _Inventario;
        public InventarioController(Iinventario inventario)
        {
            _Inventario = inventario;
        }

        // GET: Administrar Insumo/Crear
        public IActionResult Create()
        {
            ViewData["idUsuarioL"] = HttpContext.Session.GetString("idUsuario");
            return View();
        }

        // POST: Administrar Insumo/Crear

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("nombreProductoInventario,cantidadActualInventario,fechaActualizacion,idUsuario")] InventarioObjF inventario)
        {
            if (ModelState.IsValid)
            {
                _Inventario.InsertarInsumo(inventario, int.Parse(HttpContext.Session.GetString("idUsuario")));
                return RedirectToAction("Index");
            }
            return View(inventario);
        }


        // GET: Administrar Insumo/Editar
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var inventario = _Inventario.ObtenerInventarioInsumoId(Convert.ToInt32(id));
            return View(inventario);
        }

        // POST: Administrar Insumo/Editar
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("nombreProductoInventario,cantidadActualInventario, idInventario")] InventarioObj inventario)
        {
            if (id != inventario.idInventario)
            {
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                try
                {

                    _Inventario.ActualizarInsumo(inventario);
                }
                catch (Exception)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(inventario);
        }


        //// GET: Obtener Insumo
        //public IActionResult Index()
        //{
        //    return View(_Inventario.ObtenerInsumos());
        //}

        public IActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewData["idSortParm"] = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
            ViewData["CurrentFilter"] = searchString;

            var inventario = _Inventario.ObtenerInsumos();

            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            var onePageOfProducts = inventario.ToPagedList(pageNumber, 25); // will only contain 25 products max because of the pageSize

            ViewBag.OnePageOfProducts = onePageOfProducts;

            if (!String.IsNullOrEmpty(searchString))
            {
                inventario = inventario.Where(s => s.nombreProductoInventario.Contains(searchString)
                                           || s.idInventario.ToString().Contains(searchString));
            }

            switch (sortOrder)
            {
                case "id_desc":
                    inventario = inventario.OrderBy(s => s.cantidadActualInventario);
                    break;
                case "Date":
                    inventario = inventario.OrderByDescending(s => s.fechaActualizacion);
                    break;
            }

            int pageSize = 8;

            return View(inventario.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public IActionResult Export()
        {
            DataTable dt = new DataTable("Grid");
            dt.Columns.AddRange(new DataColumn[3] {
                                        new DataColumn("Nombre"),
                                        new DataColumn("Cantidad"),
                                        new DataColumn("Fecha"),
                                       

            });

            var Inventario = _Inventario.ObtenerInsumos().ToList();

            foreach (var item in Inventario)
            {
                dt.Rows.Add(item.nombreProductoInventario, item.cantidadActualInventario, item.fechaActualizacion);
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InventarioReporte.xlsx");
                }
            }
        }


        // GET: Administrar Insumo/Eliminar
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var inventario = _Inventario.ObtenerInventarioInsumoId(Convert.ToInt32(id));

            if (inventario == null)
            {
                return RedirectToAction("Index");
            }

            return View(inventario);
        }

        // POST: Administrar Insumo/Eliminar

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _Inventario.EliminarInsumo(id);
            return RedirectToAction(nameof(Index));
        }
    }
}

