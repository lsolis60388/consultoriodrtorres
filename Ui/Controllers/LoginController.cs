﻿using Microsoft.AspNetCore.Mvc;
using Repository;
using Entities;
using System.Data;
// AUTHENTICATION COOKIE
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Spreadsheet;
using PaulMiami.AspNetCore.Mvc.Recaptcha;

namespace Ui.Controllers
{
    [Route("login")]
    public class LoginController : Controller
    {
        private readonly ILogin _iLogin;
        private readonly IBitacora _ibitacora;
        public LoginController(ILogin iLogin, IBitacora Ibitacora)
        {
            _iLogin = iLogin;
            _ibitacora = Ibitacora;
        }
        [Route("")]
        //[Route("~/")]
        [Route("index")]
        public IActionResult Index()
        {
            return View();
        }
        //Autenticación del usuario

        
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> IndexAsync([Bind("correoUsuario,passUsuario")] LoginObj loginObj)
        {
            
            var account = _iLogin.ObtenerUsuarioByID(loginObj);
            if (account != null)
            {
                if (account.olvidarUsuario == "No")
                {
                    #region AUTENTICACTION
                    var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, account.nombreUsuario),
                      new Claim(ClaimTypes.MobilePhone, account.idUsuario.ToString()),
                     new Claim(ClaimTypes.Role, account.idRolUsuario.ToString()),
                    new Claim("Correo", account.correoUsuario),
                     new Claim("id", account.idUsuario.ToString()),
                };
                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
                    #endregion
                    HttpContext.Session.SetString("Correo", account.correoUsuario.ToString());
                    HttpContext.Session.SetString("idUsuario", account.idUsuario.ToString());
                    _ibitacora.RegistroIngresoSistema(account);
                    return Redirect("~/Paciente/Index");
                }
                else
                {
                    HttpContext.Session.SetString("idUsuario", loginObj.idUsuario.ToString());
                    HttpContext.Session.SetString("Correo", loginObj.correoUsuario.ToString());
                    return Redirect("~/Usuario/Change");
                }
            }
            else
            {
                var accountForget = _iLogin.ObtenerCorreoUsuarioId(loginObj);
                if (accountForget != null)
                {
                    int sumarIntento = accountForget.intentosUsuario + 1;        

               _iLogin.ActualizarIntento(accountForget.correoUsuario, sumarIntento);

                if (sumarIntento >= 3)
                {
                     _iLogin.ActualizarCambiarPassBloqueado(loginObj.correoUsuario);
                    ViewBag.msg = "Usuario bloqueado, excedió la cantidad de intentos.";
                }
                else {
                    ViewBag.msg = "Error, las credenciales no son correctas";
                    return View("Index");

                }
                  }

                return View("Index");
            }
        }
        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            //HttpContext.Session.Remove("idRol");
            //3.- CONFIGURACION DE LA AUTENTICACION
            #region AUTENTICACTION
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            #endregion
            return Redirect("~/Home/Index");
        }
    }
}