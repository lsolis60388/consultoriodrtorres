﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository;
namespace Ui.Controllers
{


    [Authorize(Roles = "1")]
    public class OdontogramaController : Controller
    {
        private readonly IOdontograma _Odontograma;
        public OdontogramaController(IOdontograma odontograma)
        {
            _Odontograma = odontograma;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewData["UserName"] = HttpContext.Session.GetString("UserName");
            ViewData["idUsuario"] = HttpContext.Session.GetString("idUsuario");
            ViewData["StringMessage"] = HttpContext.Session.GetString("stringValues");
            return View();
        }
        [HttpPost]
        public ActionResult Index(string jsonData)
        {
            if (jsonData == "[]")
            {
                return Json("Error, seleccione alguna pieza dental");
            }
            else
            {
                var idUsaurio = HttpContext.Session.GetString("idUsuario");
                _Odontograma.InsertarOdontograma(jsonData, idUsaurio);
                return Json(new { redirectToUrl = Url.Action("Index", "Paciente") });
            }
        }
    }
}