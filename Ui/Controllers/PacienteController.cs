﻿using Microsoft.AspNetCore.Mvc;

using Entities;
using Repository;
using X.PagedList;
using ClosedXML.Excel;
using System.Data;
using Microsoft.AspNetCore.Authorization;

namespace Ui.Controllers
{

    [Authorize]
    public class PacienteController : Controller
    {
        private readonly IPaciente _Paciente;
        

        public PacienteController(IPaciente paciente)
        {
            _Paciente = paciente;
        }

        // GET: Administrar Paciente/Crear
        public IActionResult Create()
        {
            return View();
        }

        // POST: Administrar Paciente/Crear

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("idPaciente, nombrePaciente, apellido1Paciente, apellido2Paciente, idHistorialClinicoPaciente, fechaNacimientoPaciente, telefonoPaciente, direccionPaciente, generoPaciente, correoPaciente, idUsuario")] PacienteObjF paciente)
        {
            var IdValue = _Paciente.ObtenerPacienteId(Convert.ToInt32(paciente.idPaciente));
            if (IdValue == null)
            {

                if (ModelState.IsValid)
                {
                    _Paciente.InsertarPaciente(paciente);
                    return RedirectToAction("Index");
                }
                return View(paciente);
            }
            else {
                ViewBag.msg = "La cédula ya existe";
                return View();
           }
        }


        // GET: Administrar Paciente/Editar
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var paciente = _Paciente.ObtenerPacienteId(Convert.ToInt32(id));
            return View(paciente);
        }

        // POST: Administrar Paciente/Editar
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("nombrePaciente, apellido1Paciente, apellido2Paciente, idHistorialClinicoPaciente, fechaNacimientoPaciente, telefonoPaciente, direccionPaciente, generoPaciente, correoPaciente, idPaciente")] PacienteObj paciente)
        {
            if (id != paciente.idPaciente)
            {
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                try
                {

                    _Paciente.ActualizarPaciente(paciente);
                }
                catch (Exception)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(paciente);
        }


        //// GET: Obtener Paciente
        //public IActionResult Index()
        //{
        //    return View(_Paciente.ObtenerPaciente());
        //}

        public IActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewData["idSortParm"] = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
            ViewData["CurrentFilter"] = searchString;

            var paciente = _Paciente.ObtenerPaciente();

            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            var onePageOfProducts = paciente.ToPagedList(pageNumber, 25); // will only contain 25 products max because of the pageSize

            ViewBag.OnePageOfProducts = onePageOfProducts;

            if (!String.IsNullOrEmpty(searchString))
            {
                paciente = paciente.Where(s => s.nombrePaciente.Contains(searchString)
                                           || s.idPaciente.ToString().Contains(searchString));
            }

            switch (sortOrder)
            {
                case "id_desc":
                    paciente = paciente.OrderBy(s => s.nombrePaciente);
                    break;
                case "Date":
                    paciente = paciente.OrderByDescending(s => s.idPaciente);
                    break;
            }

            int pageSize = 8;

            return View(paciente.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public IActionResult Export()
        {
            DataTable dt = new DataTable("Grid");
            dt.Columns.AddRange(new DataColumn[9] {
                                        new DataColumn("Cédula"),
                                        new DataColumn("Nombre Paciente"),
                                        new DataColumn("Primer Apellido"),
                                        new DataColumn("Segundo Apellido"),
                                        new DataColumn("Fecha Nacimiento Paciente"),
                                        new DataColumn("Número Telefónico"),
                                        new DataColumn("Dirección"),
                                        new DataColumn("Genero"),
                                        new DataColumn("Correo electrónico"),

            });

            var Paciente = _Paciente.ObtenerPaciente().ToList();

            foreach (var item in Paciente)
            {
                dt.Rows.Add(item.idPaciente, item.nombrePaciente, item.apellido1Paciente, item.apellido2Paciente, item.fechaNacimientoPaciente, item.telefonoPaciente, item.direccionPaciente, item.generoPaciente, item.correoPaciente);
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "PacienteReporte.xlsx");
                }
            }
        }


        // GET: Administrar Paciente/Eliminar
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var paciente = _Paciente.ObtenerPacienteId(Convert.ToInt32(id));

            if (paciente == null)
            {
                return RedirectToAction("Index");
            }

            return View(paciente);
        }

        // POST: Administrar Paciente/Eliminar

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _Paciente.EliminarPaciente(id);
            return RedirectToAction(nameof(Index));
        }






        public IActionResult HC(int id)
        {
            var paciente = _Paciente.ObtenerPacienteId(Convert.ToInt32(id));

            var f = new PacienteObjF();
            f.idPaciente = id;
            return RedirectToAction("Index", "HistorialClinico", paciente);


            // }

            // return View(paciente);
        }
    }
}

