﻿using Microsoft.AspNetCore.Mvc;
using Entities;
using Repository;
using X.PagedList;
using ClosedXML.Excel;
using System.Data;
using Microsoft.AspNetCore.Authorization;
namespace ConsultorioDrTorres.Controllers
{
    [Authorize(Roles = "1")]
    public class RecetaController : Controller
    {
        private readonly Ireceta _Receta;
        public RecetaController(Ireceta receta)
        {
            _Receta = receta;
        }
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Crear([Bind("nombreDoctor,codigoDoctor,detalleReceta,idHistorialClinico")] RecetaObjF receta)
        {
            if (ModelState.IsValid)
            {
                receta.idHistorialClinico = int.Parse(HttpContext.Session.GetString("idUsuario"));
                _Receta.InsertarReceta(receta, receta.idHistorialClinico);
                return RedirectToAction("Index");
            }
            return View(receta);
        }
        public IActionResult Editar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var receta = _Receta.ObtenerRecetaId(Convert.ToInt32(id));
            return View(receta);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Editar(int id, [Bind("nombreDoctor,codigoDoctor,detalleReceta,idHistorialClinico,idRecetas")] RecetaObj receta)
        {
            if (id != receta.idRecetas)
            {
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _Receta.ActualizarReceta(receta);
                }
                catch (Exception)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(receta);
        }
        public IActionResult Eliminar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var receta = _Receta.ObtenerRecetaId(Convert.ToInt32(id));
            if (receta == null)
            {
                return RedirectToAction("Index");
            }
            return View(receta);
        }
        [HttpPost, ActionName("Eliminar")]
        [ValidateAntiForgeryToken]
        public IActionResult Eliminar(int id)
        {
            _Receta.EliminarReceta(id);
            return RedirectToAction(nameof(Index));
        }
        public IActionResult Detalles(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var receta = _Receta.ObtenerRecetaId(Convert.ToInt32(id));
            if (receta == null)
            {
                return RedirectToAction("Index");
            }
            return View(receta);
        }
        public IActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            ViewData["idSortParm"] = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
            ViewData["CurrentFilter"] = searchString;
            var recetas = _Receta.ObtenerVistaReceta(int.Parse(HttpContext.Session.GetString("idUsuario")));
            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            var onePageOfProducts = recetas.ToPagedList(pageNumber, 25); // will only contain 25 products max because of the pageSize
            ViewBag.OnePageOfProducts = onePageOfProducts;
            if (!String.IsNullOrEmpty(searchString))
            {
                recetas = recetas.Where(s => s.nombrePaciente.Contains(searchString)
                                           || s.detalleReceta.Contains(searchString) || s.idPaciente.ToString().Contains(searchString));
            }
            switch (sortOrder)
            {
                case "id_desc":
                    recetas = recetas.OrderBy(s => s.idPaciente);
                    break;
                case "Date":
                    recetas = recetas.OrderByDescending(s => s.fechaCreacion);
                    break;
            }
            int pageSize = 8;
            return View(recetas.ToPagedList(pageNumber, pageSize));
        }
        [HttpPost]
        public IActionResult Export()
        {
            DataTable dt = new DataTable("Grid");
            dt.Columns.AddRange(new DataColumn[6] { new DataColumn("Codigo Del Doctor"),
                                        new DataColumn("Detalle Receta"),
                                        new DataColumn("ID del Paciente"),
                                        new DataColumn("Nombre Del Doctor"),
                                        new DataColumn("Fecha De Creacion"),
                                        new DataColumn("Nombre Del Paciente"),
            });
            var Recetas = _Receta.ObtenerVistaReceta(int.Parse(HttpContext.Session.GetString("idUsuario"))).ToList();
            foreach (var item in Recetas)
            {
                dt.Rows.Add(item.codigoDoctor, item.detalleReceta, item.idPaciente, item.nombreDoctor, item.fechaCreacion, item.nombrePaciente);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "RecetasReporte.xlsx");
                }
            }
        }


        public IActionResult Crear(int? id)
        {


            RecetaObjF receta = new RecetaObjF();
            receta.idHistorialClinico = int.Parse(HttpContext.Session.GetString("idUsuario"));
            receta.nombreDoctor = "Braulio Torres";
            receta.codigoDoctor = 443011 ;
            HttpContext.Session.GetString("UserName");



            return View(receta);
        }
    }
}


