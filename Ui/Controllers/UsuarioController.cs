﻿using Microsoft.AspNetCore.Mvc;
using System.Data;
using Entities;
using Repository;
using System.Net;
using System.Net.Mail;

namespace Ui.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly IUsuario _Usuario;
        public UsuarioController(IUsuario usuario)
        {
            _Usuario = usuario;
        }

        // GET: Administrar Usuario/Crear
        public IActionResult Create()
        {
            return View();
        }

        //Crear Usuario Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("idUsuario,nombreUsuario,correoUsuario,passUsuario,confirmPassword")] UsuarioObj usuarioObj)
        {
            var accountValue = _Usuario.ObtenerCorreoID(usuarioObj);
            if (accountValue == null)
            {
                if (ModelState.IsValid)
                {
                    _Usuario.CrearUsuario(usuarioObj);
                    return Redirect("~/Login/Index");
                }
                return View(usuarioObj);
            }
            else
            {
                ViewBag.msg = "El correo o la cédula ya existen en el sistema";
                return View();
            }
        }

        public IActionResult Forget()
        {
            return View();
        }

        //Olvidó contraseña del usuario
        [HttpPost]
        public IActionResult Forget([Bind("correoUsuario")] UsuarioObj usuarioObj)
        {
            var account = _Usuario.OlvidoContrasenna(usuarioObj);
            if (account != null)
            {
                int idRol = account.idRolUsuario;
                SmtpClient smtp = new SmtpClient("smtp.office365.com");

                smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credential =
                    new System.Net.NetworkCredential("notificaciones@torresdent.com", "3teshe-ZY");
                smtp.EnableSsl = true;
                smtp.Credentials = credential;

                MailMessage mensaje = new MailMessage("notificaciones@torresdent.com", account.correoUsuario);
                mensaje.Subject = "Olvidé mi contraseña - Clínica Dr Torres";
                mensaje.Body = String.Format("<p>Estimado paciente,</p>\n<p>Se ha realizado una petici&oacute;n de \"<span style=\"text-decoration: underline;\">Olvid&eacute; contrase&ntilde;a</span>\", los datos de su cuenta son los siguientes:</p>\n<p>Correo: {0}</p>\n<p>Contrase&ntilde;a: {1}</p>\n<p>En el siguiente enlace podr&aacute; ingresar a la p&aacute;gina web: <a title=\"Cl&iacute;nica Dr Torres\" href=\"https://www.torresdent.com/login/index\" target=\"_blank\">Cl&iacute;nica Dr Torres</a></p>\n<p><strong>Por favor no comparta sus datos con nadie.</strong></p>", account.correoUsuario, account.TempPassUsuario);
                mensaje.IsBodyHtml = true;
                smtp.Send(mensaje);

                return Redirect("~/Login/Index");
            }
            else
            {
                ViewBag.msg = "Error, el correo no existe";
                return View();
            }
        }

        public IActionResult Change()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Change([Bind("passUsuario,confirmPassword")] UsuarioObj usuarioObj)
        {
            if (usuarioObj.passUsuario != null)
            {
                try
                {
                    string mail = HttpContext.Session.GetString("Correo");
                    _Usuario.CambiarContrasenna(usuarioObj, mail);
                }
                catch (Exception)
                {
                    throw;
                }
                return Redirect("~/Login/Index");
            }
            return Redirect("~/Login/Index");
        }

    }
}

