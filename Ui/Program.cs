﻿using Entities;
using Repository;
using Microsoft.AspNetCore.Authentication.Cookies;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;
using PaulMiami.AspNetCore.Mvc.Recaptcha;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();


builder.Services.AddRecaptcha(new RecaptchaOptions
{
    SiteKey = builder.Configuration["Recaptcha:SiteKey"],
    SecretKey = builder.Configuration["Recaptcha:SecretKey"]
});


// esta directiva hace que los valores implicitos que no son requeridos en las clases de objetos con data anotations no sean requeridos como
//validos a la hora de hacer un Model.valid en el controlador

builder.Services.AddControllers(
    options => options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true);


builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(option => {
        option.LoginPath = "/Login/Index";
        option.ExpireTimeSpan = TimeSpan.FromMinutes(20);
        option.AccessDeniedPath = "/Home/Index";
    });



// registering Service in IOC container
builder.Services.AddTransient<IEventos, EventosTotal>();



builder.Services.AddTransient<Ireceta, RecetaTotal>();

builder.Services.AddTransient<Iinventario, InventarioTotal>();
builder.Services.AddTransient<IUsuario, UsuarioTotal>();

builder.Services.AddTransient<IBitacora, BitacoraTotal>();




builder.Services.AddSession();

builder.Services.AddScoped<ILogin, LoginTotal>();
builder.Services.AddTransient<IPaciente, PacienteTotal>();

builder.Services.AddTransient<Ihistorialclinico, HistorialTotal>();
builder.Services.AddTransient<IOdontograma, OdontogramaTotal>();

var app = builder.Build();


string connString = builder.Configuration.GetConnectionString("DatabaseConnection");

MySQLString.Valor = connString;








// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseSession();

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

//auten faru
app.UseAuthentication();
//NO mover de aqui porfa



app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

